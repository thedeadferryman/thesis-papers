# Дорожная карта

- [ ] ==29/10== Черновик лит. обзора (+ понять, как его сделать для моей темы)
- [ ] ==02/11== Финальная версия лит. обзора
- [ ] Подготовка драфтовой реализации
   - [ ] ==05/11== Разобраться, как парсить C++-исходники (libclang/другая либа/самописный парсер)
   - [ ] ==07/11== Примерная архитектура приложения (набросать схему)
   - [ ] ==20/11== Написать черновой парсер C++
   - [ ] ==3/12== Написать генератор C-кода без учета STL/сторонних библиотек
   - [ ] ==17/12== Написать генератор C-кода для STL-контейнеров
- [ ] ==28/12== Полировка реализации
- [ ] Работа с текстом дипломной работы (сроки определить сложно, т. к. пока не вполне понятно, как именно надо будет описывать работу)
